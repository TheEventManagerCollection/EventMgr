from requests import get, put, delete
from requests.auth import HTTPBasicAuth

def add_volunteer():
	data = {}
	data['first_name'] = "Jason"
	data['last_name'] = "Zhang"

	result = put('http://localhost:5000/volunteers', data=data, auth=HTTPBasicAuth("bjs", "password")).json()
	print(result)
	
def get_volunteer():
	data= {} 
	data['volunteer_id'] = 1
	
	result = get('http://localhost:5000/volunteers', data=data, auth=HTTPBasicAuth("bjs", "password")).json()
	print(result)
	
def delete_volunteer():
	data = {}
	data['volunteer_id'] = 3
	
	result = delete('http://localhost:5000/volunteers', data=data, auth=HTTPBasicAuth('bjs', 'password')).json()
	print(result)
