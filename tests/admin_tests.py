from requests import put, get, delete
from requests.auth import HTTPBasicAuth

def add_admin():
	info = {}
	
	info['first_name'] = 'Billy'
	info['last_name'] = 'Joe'
	
	info['username'] = 'bjs'
	info['email'] = 'bjs@billybobjoe.bbj'
	
	info['password'] = 'password'
	info['auth_username'] = 'bjs'
	info['auth_password'] = 'password'
	
	result = put('http://localhost:5000/admin', data=info).json()
	
	print(result)

def get_admin():
	data = {}
	data['admin_id'] = 1
	data = get('http://localhost:5000/admin', data=data, auth=HTTPBasicAuth('bjs', 'password')).json()
	
	print(data)
	
def user_list():
	data = get('http://localhost:5000/admin/user_list', auth=HTTPBasicAuth('bjs', 'password')).json()
	
	print(data)
	
def admin_list():
	data = get('http://localhost:5000/admin/admin_list', auth=HTTPBasicAuth('bjs', 'password')).json()
	
	print(data)
	
def delete_user():
	user_id= {}
	user_id['user_id'] = 2
	
	data = delete('http://localhost:5000/admin/delete_user', data=user_id, auth=HTTPBasicAuth('bjs', 'password'))
	print(data)
	
def delete_admin():
	data = {}
	data['admin_id'] = 1
	
	data = delete('http://localhost:5000/admin', data=data, auth=HTTPBasicAuth('bjs', 'password'))
	
	print(data)


	
	
