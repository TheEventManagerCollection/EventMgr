import config
from flask import request
from flask_restful import Resource
from app import api

current_config = config.Config()

class EventREST(Resource):
	def get(self):
		details = {}
		
		details["event_name"] = current_config.app_event_name
		details["event_short_description"] = current_config.app_event_short_description
		details["event_long_description"] = current_config.app_event_long_description
		
		return details
	
	
api.add_resource(EventREST, '/event_info')


