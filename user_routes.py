from models import *

from flask import request
from flask_restful import Resource, Api
from app import db
from app import api

import json


class UserREST(Resource):
	def get(self):
		user_id = request.form['user_id']
		return User.query.filter_by(id=user_id).first().asdict()
		
	def put(self):
		result = {}
		try:
			
			print(request.form['interests'])
			
			first_name = request.form['first_name']
			last_name = request.form['last_name']
			interests = request.form['interests'] # YOU MUST USE json.dumps when you send the data here.
			
			n_user = User(first_name=first_name, last_name=last_name, interests=interests)
			
			db.session.add(n_user)
			db.session.commit()
			
			result["success"] = True
			result["user_id"] = n_user.id
			
			return result
		
		except Exception:
			result["success"] = False
			return result
			
	
		

		
api.add_resource(UserREST, "/user")
		
		
		
		

	
