from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Api
from flask_migrate import Migrate
from flask_httpauth import HTTPBasicAuth

from config import Config


app = Flask(__name__)
app.config.from_object(Config)

db = SQLAlchemy(app)
migrate = Migrate(app, db)
api = Api(app)
auth = HTTPBasicAuth()

from models import *
import user_routes 
import admin_routes 
import roomservice_routes 
import volunteer_routes 
import event_routes 

@auth.verify_password
def verify_admin_password(username, password):
	if username == None or password == None:
		return False
	
	admin = Admin.query.filter_by(username=username).first()
	
	if admin==None:
		return False
	
	return admin.check_password(password)







		
