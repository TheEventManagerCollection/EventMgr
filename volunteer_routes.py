from app import api, auth, db
from models import *
from flask import request
from flask_restful import Resource

import json




class VolunteerREST(Resource):
	
	def get(self):
		volunteer_id = request.form['volunteer_id']
		
		volunteer = Volunteer.query.filter_by(id=volunteer_id).first()
		
		return volunteer.asdict()
	
	
	@auth.login_required
	def put(self):
		result = {}
		
		first_name = request.form['first_name']
		last_name = request.form['last_name']
		
		new_volunteer = Volunteer(first_name=first_name, last_name=last_name)
		db.session.add(new_volunteer)
		db.session.commit()
		
		result["success"] = True
		
		return result, 201

	@auth.login_required
	def delete(self):
		result = {}
		
		volunteer_id = request.form['volunteer_id']
		volunteer_td = Volunteer.query.filter_by(id=volunteer_id).first()
		
		db.session.delete(volunteer_td)
		db.session.commit()
		
		result['success'] = True
		return result
		
		
		
		
	
		
		
	
	

api.add_resource(VolunteerREST, '/volunteers')
