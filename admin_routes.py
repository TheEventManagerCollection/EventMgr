from models import *

from flask import request
from flask_restful import Resource

from app import db, auth, api

import json



class ThisAdminDetailsREST(Resource):
	@auth.login_required
	def get(self):
		admin = Admin.query.filter_by(username=auth.username()).first()
		return admin.asdict()
	

class AdminListREST(Resource):
	@auth.login_required
	def get(self):
		admins = Admin.query.all()
		admin_jsonified_list = []
		
		for admin in admins:
			admin_jsonified_list.append(admin.asdict())
		
		return admin_jsonified_list
		
class AdminREST(Resource):
	
	def get(self): # get an admin's details from their id
		admin_id = request.form['admin_id']
		
		admin = Admin.query.filter_by(id=admin_id).first()
		return admin.asdict()
	
	
	def put(self): # add an admin
		result = {}
		
		first_name = request.form["first_name"]
		last_name = request.form["last_name"]
		username = request.form["username"]
		password = request.form["password"]
		email = request.form["email"]
		
		
		
		if Admin.query.count() != 0:
			result["success"] = False
			result["msg"] = "An admin already exists!"
			
			admin = Admin.query.filter_by(username=request.form["auth_username"]).first()
			
			if admin == None or request.form["auth_username"] == None or request.form["auth_password"] == None:
				result["success"] = False
				result["msg"] = "Access denied"
				
			if admin.check_password(request.form["auth_password"]):
				result = self.add_admin(first_name, last_name, username, email, password)
				return result
			
		
		result = self.add_admin(first_name, last_name, username, email, password)
		return result
			
			
			
		
		
		
			
		
	def add_admin(self, _first_name, _last_name, _username, _email, _password):
		
		result = {}
		
		hpassword = Admin.hash_password(password=_password, email=_email)
		
		new_admin = Admin(first_name=_first_name, 
							last_name=_last_name, 
							username=_username,
							email=_email,
							hpassword=hpassword)
		
		db.session.add(new_admin)
		db.session.commit()
		
		result["success"] = True
		
	
			
		return result
		
	@auth.login_required
	def delete(self):
		result = {}
		try:
			admin_id = request.form["admin_id"]
			
			admin_to_delete = Admin.query.filter_by(id=admin_id).first()
			
			db.session.delete(admin_to_delete)
			db.session.commit()
			
			result["success"] = True
		except Exception: 
			result["success"] = False
			
		return result
			
		
	
			
		
		
			
			
				

class UserListREST(Resource):
	@auth.login_required # login required because we don't want random people getting the entire list of users
	def get(self):
		users = User.query.all()
		users_jsonified_list = []
		
		for user in users:
			users_jsonified_list.append(user.asdict())
		
		return users_jsonified_list
		
class VolunteerListREST(Resource):
	@auth.login_required # login required because we don't want random people getting the entire list of users
	def get(self):
		volunteers = Volunteer.query.all()
		volunteers_jsonified_list = []
		
		for volunteer in volunteers:
			volunteers_jsonified_list.append(volunteer.asdict())
		
		return volunteers_jsonified_list
		

	
	
class DeleteUserREST(Resource):
	@auth.login_required
	def delete(self):
		try:
			result = {}
		
			user_id = request.form['user_id']
			
			user_to_delete = User.query.filter_by(id=user_id).first()
			
			print(user_to_delete.first_name)
			
			db.session.delete(user_to_delete)
			db.session.commit()
			
			result["success"] = True
		except Exception:
			result["success"] = False
			
			
		return result
		


	

api.add_resource(ThisAdminDetailsREST, '/admin/this_details') #works 
api.add_resource(AdminREST, '/admin') #works
api.add_resource(AdminListREST, '/admin/admin_list') 
api.add_resource(UserListREST, '/admin/user_list') #works
api.add_resource(DeleteUserREST, '/admin/delete_user') #works
api.add_resource(VolunteerListREST, '/admin/volunteer_list') #works
		
		
	


