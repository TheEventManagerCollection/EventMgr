from app import api, auth, db
from models import *
from flask import request
from flask_restful import Resource

import json

class RoomServiceREST(Resource):
	def put(self):
		try:	
			result = {}
			
			user_id =  request.form["user_id"]
			user = User.query.filter_by(id=user_id).first()
			
			rs_desc = request.form["description"]
			
			new_rs = RoomService(requester=user, description=rs_desc)
			
			db.session.add(new_rs)
			db.session.commit()
			
			result["success"] = True
			
				
			return result
		except Exception: 
			result["success"] = False
			return result
			
		
	
	def get(self):
		request_id = request.form["request_id"]
		
		rs_request = RoomService.query.filter_by(id=request_id).first()
		return rs_request.asdict()
		
	def delete(self):
		try:
			result = {}
			
			
			request_id = request.form["request_id"]
			
			rs_request = RoomService.query.filter_by(id=request_id).first()
			
			db.session.delete(rs_request)
			db.session.commit()
			
			result["success"] = True
		
				
			
			return result
		except Exception: 
			result["success"] = False
			
class RoomServiceCountREST(Resource):
	def get(self):
		id_list = []
		
		all_requests =  RoomService.query.all()
		for rs_request in all_requests:
			id_list.append(rs_request.id)
			
		return {"id_list": id_list}
			
		
api.add_resource(RoomServiceREST, '/room_service')
api.add_resource(RoomServiceCountREST, '/room_service_count')
		
		
		
		
		
		
		
