from flask_restful import Resource, Api
from app import db

import json
import hashlib

class User(db.Model): # these are actually registrants, but whatever
	
	__tablename__ = "users"
	
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	
	first_name = db.Column(db.String(256), index=True)
	last_name = db.Column(db.String(256), index=True)
	
	interests = db.Column(db.String(99999999999999999), index=True)
	
	room_service_requests = db.relationship('RoomService', backref='requester', lazy='dynamic')
	
	def asdict(self):
		ndict = {}
		ndict["id"] = self.id
		ndict["first_name"] = self.first_name
		ndict["last_name"] = self.last_name
		ndict["interests"] = self.interests
		
		return ndict
		
	def get_interests_list(self):
		return json.loads(self.interests)
	
	
	
class Admin(db.Model):
	__tablename__ = "admins"
	
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	
	first_name = db.Column(db.String(256), index=True)
	last_name = db.Column(db.String(256), index=True)
	
	username = db.Column(db.String(256), index=True)
	hpassword = db.Column(db.String(256)) # anything added here MUST be hashed with hashlib.sha256(password.encode('utf-8') + email.encode('utf-8')).hexdigest() - either way passwords won't work unless the hashes match
	
	email = db.Column(db.String(9999))
	
	# password + email EMAIL after password in salt
	
	def asdict(self):
		ndict = {}
		ndict['id'] = self.id
		ndict['first_name'] = self.first_name
		ndict['last_name'] = self.last_name
		ndict['username'] = self.username
		ndict['email'] = self.email
		ndict['hpassword'] = self.hpassword
		
		return ndict
		
	
	def check_password(self, password):
		return hashlib.sha256(password.encode('utf-8') + self.email.encode('utf-8')).hexdigest() == self.hpassword
		
	@staticmethod
	def hash_password(password, email):
		return hashlib.sha256(password.encode('utf-8') + email.encode('utf-8')).hexdigest()

class RoomService(db.Model):
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	
	requester_id = db.Column(db.Integer, db.ForeignKey('users.id'))
	description = db.Column(db.UnicodeText())
	
	def asdict(self):
		ndict = {}
		ndict["id"] = self.id
		ndict["requester_id"] = self.requester_id
		ndict["description"] = self.description
		
		return ndict
		
class Volunteer(db.Model):
	__tablename__ = 'volunteers'
	
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	
	first_name = db.Column(db.String(256), index=True)
	last_name = db.Column(db.String(256), index=True)
	
	def asdict(self):
		ndict = {}
		ndict["id"] = self.id
		ndict["first_name"] = self.first_name
		ndict["last_name"] = self.last_name
		
		return ndict
		
		
		
		
	
	
	
	
	
	
		

	

		
	
