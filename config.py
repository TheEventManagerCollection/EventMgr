
class Config():
	
	
	
	app_db_vendor = "mysql"
	app_db_user = "default_u"
	app_db_password = "letmeinmysql"
	app_db_host = "localhost"
	app_db_name = "event_mgr_db"
	
	
	app_event_name = "UnHacks"
	app_event_long_description = "UnHacks is the wrong type of hackathon for anyone. It has no rules and basically <b>it's really hard to win</b>. We have a lot of amenities and it is a 48 houur hackathon." 
	app_event_short_description = "Not a hackathon that *you* want to be at!" 
	
	
	
	
	
	
	#*************** DO NOT EDIT *****************
	
	SQLALCHEMY_DATABASE_URI = f"{app_db_vendor}://{app_db_user}:{app_db_password}@{app_db_host}/{app_db_name}"
	SQLALCHEMY_TRACK_MODIFICATIONS = False
	
	
	
	
	
	
	
	#*********************************************


